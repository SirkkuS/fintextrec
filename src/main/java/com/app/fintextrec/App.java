package com.app.fintextrec;
import java.util.ArrayList;
import java.util.List;


public class App {

    // Set the path for connecting to Solr.
    public static final String SOLRPATH = "";
    // Set the path for connecting to the database.
    public static final String DBPATH = "";
    // Set the name of the database.
    public static final String DBNAME = "";
    // Set the name of the table that contains original texts.
    public static final String MAIN_TABLE = "";
    // Set the path to the text file that contains stopwords.
    // If there is no file, the value should be an empty string.
    public static final String STOPWORDSPATH = "";

    public static void main(String[] args) {

        // Set the core name.
        String coreName;
        //Document.deleteAllDocuments(coreName);
        //Document.indexAll(coreName);

        // Set the flags and the table name which contains processed texts.
        String tableName;
        boolean lemmatize;
        boolean allowInflectedForm;
        boolean removeStopWords;
        /*
        ProcessedText.processAll(tableName,
                                 lemmatize,
                                 allowInflectedForm,
                                 removeStopWords);
        */

        ArrayList solr_suggestions;
        List mysql_suggestions;
        // Set the max number of suggestions.
        int limit;
        // Set the id of the source document.
        int id;
        //solr_suggestions = Document.getSuggestions(coreName, id, limit);
        //mysql_suggestions = ProcessedText.getSuggestions(tableName, id, limit);
    }
}