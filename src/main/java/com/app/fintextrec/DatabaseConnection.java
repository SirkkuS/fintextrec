package com.app.fintextrec;
import com.mysql.cj.jdbc.ConnectionImpl;
import java.sql.DriverManager;

public class DatabaseConnection {

    // Set the username and password.
    private static final String USER = "";
    private static final String PASS = "";

    // Connects to the database.
    public static ConnectionImpl connect() {
        ConnectionImpl c = null;
        try {
            String dburl = App.DBPATH + App.DBNAME;
            Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
            c = (ConnectionImpl) DriverManager.getConnection(dburl, USER, PASS);
        } catch (Exception e) {
            System.err.println("Cannot connect to the database server.");
        }
        return c;
    }
}