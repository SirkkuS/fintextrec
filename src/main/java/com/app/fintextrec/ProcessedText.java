package com.app.fintextrec;
import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import org.puimula.libvoikko.Analysis;
import org.puimula.libvoikko.Voikko;


public class ProcessedText {

    // Fulltext search for suggestions
    public static List getSuggestions(String tableName, int id, int limit) {

        // Results also include the id, which will get removed from suggestions.
        int new_limit = limit + 1;
        List<Integer> suggestions = new ArrayList<>();
        // Gets the processed text that belongs to the id.
        String processedText = getProcessedText(tableName, id);

        if (!"".equals(processedText)) {
            try (Connection c = DatabaseConnection.connect()) {
                String query = "SELECT response_id FROM " + tableName
                    + " WHERE MATCH (processed_text)"
                    + " AGAINST (?)"
                    + " LIMIT " + new_limit + ";";
                PreparedStatement preparedStmt = c.prepareStatement(query);
                preparedStmt.setString(1, processedText);
                ResultSet rs = preparedStmt.executeQuery();

                while (rs.next()) {
                    int responseId = rs.getInt("response_id");
                    if (responseId != id) {
                        System.out.println(responseId);
                        suggestions.add(responseId);
                    }
                }
                c.close();
            } catch (Exception e) {
                System.err.println("Exception in FT query");
            }
        }
        return suggestions;
    }


    // Gets processed text from the database.
    public static String getProcessedText(String tableName, int id) {
        String processedText = "";
        try (Connection c = DatabaseConnection.connect()) {
            String query = "SELECT processed_text FROM " + tableName
                + " where response_id = ?";
            PreparedStatement preparedStmt = c.prepareStatement(query);
            preparedStmt.setInt(1, id);
            ResultSet rs = preparedStmt.executeQuery();
            while (rs.next()) {
                processedText = rs.getString("processed_text");
            }
        } catch (Exception e) {
            System.err.println("Exception in retrieving data.");
        }
        return processedText;
    }

    // Processes a text so it can be inserted to the database.
    public static String processText(String text,
                                     boolean lemmatize,
                                     boolean allowInflectedForm,
                                     String stopWords) {

        Voikko voikko = new Voikko("fi");
        String processedText = "";

        // Removes everything else but hyphen, letters and numbers
        String pattern = "[.,:;!?\"'()\\[\\]\\{\\}/\\\\]";
        String preprocessedText = text.replaceAll(pattern, "");
        // Tokenizes the string.
        String wordsArray[] = preprocessedText.split(" ");

        for (int i = 0; i < wordsArray.length; ++i) {
            String word = wordsArray[i].toLowerCase();
            // stopWords contains the stop words or is an empty string.
            if (stopWords.contains(word)) {
                continue;
            }
            // Result will be a string of baseform(s) or the original word.
            String result;
            if (lemmatize) {
                result = analyze(voikko, word);
                if (allowInflectedForm && result.equals("")) {
                    result = word;
                }
            }
            else {
                result = word;
            }
            processedText = processedText.concat(result + " ");
        }
        return processedText.trim();
    }

    // Voikko analyzes a preprocessed string.
    // The morphological analysis can return more than one baseform.
    public static String analyze(Voikko voikko, String word) {
        String result = "";
        List<Analysis> analysis = voikko.analyze(word);

        if (!analysis.isEmpty()) {
            for (Analysis a : analysis) {
                String baseform = a.get("BASEFORM").toLowerCase();
                if (!(result.contains(baseform))) {
                    result = result.concat(baseform + " ");
                }
            }
        }
        return result.trim();
    }

    // Processes all original texts in the table.
    public static void processAll(String tableName,
                                  boolean lemmatize,
                                  boolean allowInflectedForm,
                                  boolean removeStopWords) {

        String stopWords = "";
        if (removeStopWords) {
            stopWords = getStopWords(stopWords);
        }

        try (Connection c = DatabaseConnection.connect()) {
            String query = "SELECT * FROM " + App.MAIN_TABLE;
            PreparedStatement preparedStmt = c.prepareStatement(query);
            ResultSet rs = preparedStmt.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");
                String text = rs.getString("response_text");
                if (text != null) {
                    String processedText = processText(text,
                                                       lemmatize,
                                                       allowInflectedForm,
                                                       stopWords);
                    insertProcessedText(tableName, id, processedText);
                }
            }
        } catch (Exception e) {
            System.err.println("Exception in processing all items.");
        }
    }


    // Inserts a processed text to the table.
    public static void insertProcessedText(String tableName, int id,
                                           String processedText) {

        try (Connection c = DatabaseConnection.connect()) {
            String query = "INSERT INTO " + tableName
                + " (response_id, processed_text) VALUES(?, ?)"
                + " ON DUPLICATE KEY UPDATE response_id=?, processed_text=?";

            PreparedStatement stmt = c.prepareStatement(query);
            stmt.setInt(1, id);
            stmt.setString(2, processedText);
            stmt.setInt(3, id);
            stmt.setString(4, processedText);
            stmt.execute();
            c.close();
        } catch (Exception e) {
            System.err.println("Exception in updating data.");
        }
    }


    // Inserts stop words into a set from a file.
    public static String getStopWords(String stopWords) {

        if (!"".equals(App.STOPWORDSPATH)) {
            File file = new File(App.STOPWORDSPATH);
            try (Scanner reader = new Scanner(file)) {
                while (reader.hasNextLine()) {
                    String line = reader.nextLine();
                    if (!line.trim().startsWith("#")) {
                        stopWords = stopWords.concat(line.trim() + " ");
                    }
                }
            } catch (Exception e) {
                System.err.println("Exception in reading the file.");
            }
        }
        return stopWords;
    }
}
