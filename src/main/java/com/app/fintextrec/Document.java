package com.app.fintextrec;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;


public class Document {

    // MoreLikeThis query for suggestions
    public static ArrayList getSuggestions(String coreName, int id, int limit) {

        ArrayList<Object> suggestions = new ArrayList<>();

        try {
            String solrurl = App.SOLRPATH + coreName;
            SolrClient solr =
                new HttpSolrClient.Builder(solrurl).build();
            SolrQuery query = new SolrQuery();
            // Specify request handler
            query.setRequestHandler("/mlt");
            query.set("mlt.fl", "response_text");
            query.set("mlt.mintf", 1);
            query.set("mlt.mindf", 1);
            query.set("mlt.minwl", 3);
            // Specifies field(s) in the results
            query.set("fl", "id");
            // Id of theource document
            query.setQuery("id:" + id);
            // Max number of documents in the response
            query.setRows(limit);
            // Execute query
            QueryResponse response = solr.query(query);
            SolrDocumentList results = response.getResults();

            if (results != null) {
                for (SolrDocument doc : results) {
                    Object similarDocId = doc.get("id");
                    System.out.println(similarDocId);
                    suggestions.add(similarDocId);
                }
            }
        } catch(Exception e) {
            System.err.println("Exception in MLT query.");
        }
        return suggestions;
    }


    // Adds a document to the index.
    public static void addDocument(String coreName, int id, String text) {

        String idString = Integer.toString(id);

        try {
            String solrurl = App.SOLRPATH + coreName;
            SolrClient solr = new HttpSolrClient.Builder(solrurl).build();
            SolrInputDocument document = new SolrInputDocument();
            document.addField("id", idString);
            document.addField("response_text", text);
            solr.add(document);
            solr.commit();
        } catch(Exception e) {
            System.err.println("Failed to add a document to the index.");
        }
    }


    // Retrieves all the responses from the database and adds them to the index.
    public static void indexAll(String coreName) {

        try (Connection c = DatabaseConnection.connect()) {
            String query = "SELECT * FROM " + App.MAIN_TABLE;
            PreparedStatement preparedStmt = c.prepareStatement(query);
            ResultSet rs = preparedStmt.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("id");
                String text = rs.getString("response_text");
                // Removes first quotation mark.
                if (text.charAt(0) == '"') {
                    text = text.substring(1);
                }
                // Removes last quotation mark.
                if (text.charAt(text.length() - 1) == '"') {
                    text = text.substring(0, text.length() - 1);
                }
                addDocument(coreName, id, text);
            }
            c.close();
        } catch (Exception e) {
            System.err.println("Exception in indexing all items.");
        }
    }


    // Deletes all the documents from the index.
    public static void deleteAllDocuments(String coreName) {

        try {
            String solrurl = App.SOLRPATH + coreName;
            SolrClient solr = new HttpSolrClient.Builder(solrurl).build();
            solr.deleteByQuery("*");
            solr.commit();
        } catch(Exception e) {
            System.err.println("Failed to delete documents from the index.");
        }
    }
}
