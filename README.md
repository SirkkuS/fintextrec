This implementation is not intended for production use.

App.java
- Add paths for MySQL and Solr.
- Add the name of the database, the table names and the name of the core.
- Add path to the file that contains stop words.
- Add values that determine how text is preprocessed.

DatabaseConnection.java
- Add username and password.
